var popup = function(html) {
	var content = '	<div class="popup"> \
						<div class="popup__body"> \
							<div class="popup__content">' + html + '</div> \
							<div class="popup__close">Закрыть</div> \
						</div> \
					</div>';

	$('body').append(content);
}

$(function() {
	if ( !$('.counter__canvas').length ) {
		return false;
	}

	var image = new Image();

	var peoples = $('.counter').attr('data-count');

	var startCount = 2.08,
		endCount = 1.06 + (Math.PI*2-startCount),
		percent = endCount * ((peoples)/25000),
		angle = ( startCount + percent ) % (Math.PI*2),
		arrowAngle = angle > 2.08 ? angle: angle + Math.PI*2;

	var init = function () {
		image.onload = counter;
		image.src = "img/background/clock.png?" + new Date().getTime();
	}()

	function counter() {
		if (!!document.createElement('canvas').getContext) {
			var canvas = document.querySelector('.counter__canvas'),
				context = canvas.getContext('2d'),
				width = 400,
				height = 400,
				pixels = width * height;

			canvas.width = width;
			canvas.height = height;

			context.drawImage(image, 0, 0);
			var image1 = context.getImageData(0, 0, width, height);
			var imageData1 = image1.data;

			context.clearRect (0, 0, 400, 400);
			context.fillStyle= '#ffffff';
			context.strokeStyle= '#ffffff';
			context.lineWidth = 0;

			context.arc(width/2, 210, 180, Math.PI/2, angle, true);
			context.lineTo(width/2, height/2);
			context.closePath();
			context.stroke();
			context.fill();

			var image2 = context.getImageData(0, 0, width, height);
			var imageData2 = image2.data;

			context.clearRect (0, 0, 400, 400);
			context.fillStyle= '#ffffff';
			context.strokeStyle= '#ffffff';
			context.lineWidth = 0;

			context.arc(width/2, 210, 200, Math.PI/2, angle+0.1, true);
			context.lineTo(width/2, height/2);
			context.closePath();
			context.stroke();
			context.fill();

			var image3 = context.getImageData(0, 0, width, height);
			var imageData3 = image3.data;

			while (pixels--) {
				if (imageData1[(pixels * 4)+3] && (imageData2[(pixels * 4)+3] || imageData3[(pixels * 4)+3])) {
					imageData1[(pixels * 4)+3] = imageData1[(pixels * 4)+3]/3;
				}
			}
			image1.data = imageData1;
			context.putImageData(image1, 0, 0);
		}

		$('.counter__current-now').text(peoples.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
		$('.counter__progress').addClass('counter__progress_loaded');
		$('.counter__arrow').css('transform', 'rotate(' + arrowAngle + 'rad)');
	}

})

$(function() {
	$('body').on('click', '.popup:not(.popup_form) .popup__close', function() {
		$(this).closest('.popup').remove();
	})

	$('body').on('click', '.popup:not(.popup_form)', function() {
		$(this).remove();
	})

	$('body').on('click', '.popup.popup_form .popup__close', function() {
		$(this).closest('.popup').hide();
	})

	$('body').on('click', '.popup.popup_form', function() {
		$(this).hide();
	})

	$('body').on('click', '.popup__body', function(e) {
		e.stopPropagation();
	})

	$('.price__header-pdf .price__header-link').click(function() {
		var template = '<div class="popup__header">Подписка на PDF-версию</div> \
						<div class="popup__text"> \
							<p>Если Вы все время в дороге и любите планшеты, то этот вариант для Вас.</p> \
							<a href="#" class="green-button__link"> \
								<div class="green-button__time">6 мес.</div> \
								<div class="green-button__price">2000 руб</div> \
							</a> \
							<a href="#" class="green-button__link"> \
								<div class="green-button__time">12 мес.</div> \
								<div class="green-button__price">3600 руб</div> \
							</a> \
						</div>';

		popup(template);
		return false;
	})

	$('.price__header-website .price__header-link').click(function() {
		var price1 = '2400 руб',
			price2 = '4200 руб';

		if ( $('#small').is(':checked') ) {
			price1 = '1877 руб';
			price2 = '3441 руб';
		}

		var template = '<div class="popup__header">ПОДПИСКА НА журнал <span class="popup__red-highlight">+</span><br />ДОСТУП К САЙТУ</div> \
						<div class="popup__text"> \
							<p>С 2014 года сайт будет открыт только для оформивших подписку. Это позволит Вам получить доступ к сайту журнала и его архиву за последние 7 лет. Если Вы читаете журнал по дороге на работу , в метро или в автобусе, а вечером дома хотели бы его еще полистать , подумать вместе с авторами, увидеть фотографии и инфографику – это вариант для Вас.</p> \
							<a href="#" class="green-button__link"> \
								<div class="green-button__time">6 мес.</div> \
								<div class="green-button__price">' + price1 + '</div> \
							</a> \
							<a href="#" class="green-button__link"> \
								<div class="green-button__time">12 мес.</div> \
								<div class="green-button__price">' + price2 + '</div> \
							</a> \
						</div>';

		popup(template);
		return false;
	})

	$('.price__body-col_3 .price__body-header-link').click(function() {
		var template = '<div class="popup__header">Журнал <span class="popup__red-highlight">+</span><br /> Доступ к сайту <span class="popup__red-highlight">+</span><br /> Членство в клубе The New Times</div> \
						<div class="popup__text"> \
							<p>Быть членом эксклюзивного клуба The New Times  - это быть приглашенным на специальные мероприятия, дискуссии с интересными людьми, обсуждения в закрытой ветке Твиттера, ужин с  лучшими журналистами журнала и его главным редактором. </p> \
							<a href="#" class="green-button__link"> \
								<div class="green-button__time">12 мес.</div> \
								<div class="green-button__price">5000 руб</div> \
							</a> \
						</div>';

		popup(template);
		return false;
	})

	$('.price__body-col_4 .price__body-header-link').click(function() {
		var template = '<div class="popup__header">Все в одном <span class="popup__red-highlight">+</span><br /> помоги журналу!</div> \
						<div class="popup__text"> \
							<p>Этот пакет включает в себя все вышеперечисленное – бумажную и PDF версии, доступ к сайту в 2014 году и членство в клубе The New Times.</p> \
							<a href="#" class="green-button__link"> \
								<div class="green-button__time">12 мес.</div> \
								<div class="green-button__price">6200 руб</div> \
							</a> \
						</div>';

		popup(template);
		return false;
	})

	$('#small').click(function() {
		$('.js-green-button__price-1').text('1877 руб');
		$('.js-green-button__price-2').text('3441 руб');

		var template = '<div class="popup__header">цена для студентов и пенсионеров</div> \
						<div class="popup__text"> \
							<p>Если Вы хотите оформить льготную подписку, пришлите нам, пожалуйста, письмо, содержащее вариант выбранной подписки и скан документа, подтверждающий Ваше право на льготу - студенческого билета или пенсионного удостоверения. <br /><a href="mailto:podpiska2014@newtimes.ru">podpiska2014@newtimes.ru</a></p> \
						</div>';

		popup(template);
	})

	$('#regular').click(function() {
		$('.js-green-button__price-1').text('2400 руб');
		$('.js-green-button__price-2').text('4200 руб');
	})
})